function findx() {
    var x = document.getElementById("x");
    var a = parseFloat(x.value);
    if (isNaN(a)) {
        document.getElementById("answer").innerHTML = "The given value is not number";
    }
    else {
        var res = a + Math.cos(a * Math.PI / 180);
        document.getElementById("answer").innerHTML = "The given value is :-" + a.toString() + " and x+cos() value is :-" + res.toString();
    }
}
//# sourceMappingURL=x+cosx.js.map