function area_circle(){
    let radius: HTMLInputElement = <HTMLInputElement>document.getElementById("radius"); 
    let answer: HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("answer");

    var radius_val: number = +radius.value;
    
    if (isNaN(radius_val)){
        answer.innerHTML = "The given value is not a number!!!";
    }
    else{
        var a = Math.PI * Math.pow(radius_val,2);
        answer.innerHTML = "Area of circle is: "+ a.toString();
    }
}