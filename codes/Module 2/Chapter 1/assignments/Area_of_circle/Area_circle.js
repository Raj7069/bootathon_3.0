function area_circle() {
    let radius = document.getElementById("radius");
    let answer = document.getElementById("answer");
    var radius_val = +radius.value;
    if (isNaN(radius_val)) {
        answer.innerHTML = "The given value is not a number!!!";
    }
    else {
        var a = Math.PI * Math.pow(radius_val, 2);
        answer.innerHTML = "Area of circle is: " + a.toString();
    }
}
//# sourceMappingURL=Area_circle.js.map