function sin_1(){
    let s1: HTMLInputElement = <HTMLInputElement>document.getElementById("t1");

    var data: number = +s1.value;

    if(isNaN(data)){
        document.getElementById("answer").innerHTML = "Please Enter number only";
    }else{
        var num: number = Math.PI/180*(data);
        var t2: number = Math.sin(data);
        document.getElementById("answer").innerHTML = t2.toString();
    }
}
function cos_1(){
    let s1: HTMLInputElement = <HTMLInputElement>document.getElementById("t1");

    var data: number = +s1.value;

    if(isNaN(data)){
        document.getElementById("answer").innerHTML = "Please Enter number only";
    }else{
        var num: number = Math.PI/180*(data);
        var t2: number = Math.cos(data);
        document.getElementById("answer").innerHTML = t2.toString();
    }
}

function tan_1(){
    let s1: HTMLInputElement = <HTMLInputElement>document.getElementById("t1");

    var data: number = +s1.value;

    if(isNaN(data)){
        document.getElementById("answer").innerHTML = "Please Enter number only";
    }else{
        var num: number = Math.PI/180*(data);
        var t2: number = Math.tan(data);
        document.getElementById("answer").innerHTML = t2.toString();
    }
}

function sqrt1(){
    let s1: HTMLInputElement = <HTMLInputElement>document.getElementById("t1");

    var data: number = +s1.value;

    if(isNaN(data)){
        document.getElementById("answer").innerHTML = "Please Enter number only";
    }else{
        var num: number = Math.sqrt(data);
        document.getElementById("answer").innerHTML = num.toString();
    }
}

function pow1(){
    let s1: HTMLInputElement = <HTMLInputElement>document.getElementById("t1");

    var data: number = +s1.value;

    if(isNaN(data)){
        document.getElementById("answer").innerHTML = "Please Enter number only";
    }else{
        var prt: number = +prompt("Enter number:-");
        
        if(isNaN(prt)){
            document.getElementById("answer").innerHTML = "Please Enter number only";
        }else{
            var answer: number = Math.pow(data,prt);
            document.getElementById("answer").innerHTML = answer.toString();
        }
    }
}

function plus1(){
    let s1: HTMLInputElement = <HTMLInputElement>document.getElementById("t1");

    var data: number = +s1.value;

    if(isNaN(data)){
        document.getElementById("answer").innerHTML = "Please Enter number only";
    }else{
        var prt: number = +prompt("Enter number:-");
        
        if(isNaN(prt)){
            document.getElementById("answer").innerHTML = "Please Enter number only";
        }else{
            var answer: number = data + prt;
            document.getElementById("answer").innerHTML = answer.toString();
        }
    }
}

function minus1(){
    let s1: HTMLInputElement = <HTMLInputElement>document.getElementById("t1");

    var data: number = +s1.value;

    if(isNaN(data)){
        document.getElementById("answer").innerHTML = "Please Enter number only";
    }else{
        var prt: number = +prompt("Enter number:-");
        
        if(isNaN(prt)){
            document.getElementById("answer").innerHTML = "Please Enter number only";
        }else{
            var answer: number = data - prt;
            document.getElementById("answer").innerHTML = answer.toString();
        }
    }
}

function mult1(){
    let s1: HTMLInputElement = <HTMLInputElement>document.getElementById("t1");

    var data: number = +s1.value;

    if(isNaN(data)){
        document.getElementById("answer").innerHTML = "Please Enter number only";
    }else{
        var prt: number = +prompt("Enter number:-");
        
        if(isNaN(prt)){
            document.getElementById("answer").innerHTML = "Please Enter number only";
        }else{
            var answer: number = data * prt;
            document.getElementById("answer").innerHTML = answer.toString();
        }
    }
}

function div1(){
    let s1: HTMLInputElement = <HTMLInputElement>document.getElementById("t1");

    var data: number = +s1.value;

    if(isNaN(data)){
        document.getElementById("answer").innerHTML = "Please Enter number only";
    }else{
        var prt: number = +prompt("Enter number:-");
        
        if(isNaN(prt)){
            document.getElementById("answer").innerHTML = "Please Enter number only";
        }else if(+prt == 0){
            alert ("You Can't divide with 0");
        }
        else{
            var answer: number = data / prt;
            document.getElementById("answer").innerHTML = answer.toString();
        }
    }
}