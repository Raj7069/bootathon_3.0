function factorial(){
    var input: HTMLInputElement = <HTMLInputElement>document.getElementById("input_text");
    var output: HTMLInputElement = <HTMLInputElement>document.getElementById("output");
    var num: number = +input.value;
    var count: number = 1;
    var sum: number = 1 ;

    while(count<= num){
        sum = sum*count;
        count++;
    }

    output.value = "Factorial of:- "+ num+" is "+sum;
}