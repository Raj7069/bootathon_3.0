function factorial() {
    var input = document.getElementById("input_text");
    var output = document.getElementById("output");
    var num = +input.value;
    var count = 1;
    var sum = 1;
    while (count <= num) {
        sum = sum * count;
        count++;
    }
    output.value = "Factorial of:- " + num + " is " + sum;
}
//# sourceMappingURL=factorial.js.map