function Armstrong(){

    var count: number = 100;

    for(count = 100; count <= 999; count++){

        var num: number = count;
        var sum: number = 0;

        while(num > 0){
            var p: number = num % 10;
            sum += Math.pow(p,3);
            num = Math.floor(num / 10);
        }
        
        if(sum == count){
            document.getElementById("p").innerHTML += "<br/>"+count.toString();
        }
    }
}