function TriangleType() {
    var x = document.getElementById("t1");
    var y = document.getElementById("t2");
    var z = document.getElementById("t3");
    var a = +x.value;
    var b = +y.value;
    var c = +z.value;
    if (a == b && b == c) {
        document.getElementById("p1").innerHTML = "Equilateral triangle";
    }
    else if ((a == b && a != c) || (b == c && b != a) || (a == c && c != b)) {
        document.getElementById("p1").innerHTML = "Isosceles triangle";
        if (Math.pow(a, 2) == (Math.pow(b, 2) + Math.pow(c, 2)) || Math.pow(b, 2) == (Math.pow(a, 2) + Math.pow(c, 2)) || Math.pow(c, 2) == (Math.pow(b, 2) + Math.pow(a, 2))) {
            document.getElementById("p2").innerHTML = "Right Angle triangle";
        }
    }
    else if (a != b && b != c && a != c) {
        document.getElementById("p1").innerHTML = "Scalene triangle";
        if (Math.pow(a, 2) == (Math.pow(b, 2) + Math.pow(c, 2)) || Math.pow(b, 2) == (Math.pow(a, 2) + Math.pow(c, 2)) || Math.pow(c, 2) == (Math.pow(b, 2) + Math.pow(a, 2))) {
            document.getElementById("p2").innerHTML = "Right Angle triangle";
        }
    }
}
//# sourceMappingURL=TriangleType.js.map