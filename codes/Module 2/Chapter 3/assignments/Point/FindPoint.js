function Findpoint() {
    let x1 = document.getElementById("x1");
    let y1 = document.getElementById("y1");
    let x2 = document.getElementById("x2");
    let y2 = document.getElementById("y2");
    let x3 = document.getElementById("x3");
    let y3 = document.getElementById("y3");
    let m = document.getElementById("m");
    let n = document.getElementById("n");
    var a = +x1.value;
    var b = +y1.value;
    var c = +x2.value;
    var d = +y2.value;
    var e = +x3.value;
    var f = +y3.value;
    var g = +m.value;
    var h = +n.value;
    if (isNaN(a) || isNaN(b) || isNaN(c) || isNaN(d) || isNaN(e) || isNaN(f) || isNaN(g) || isNaN(h)) {
        document.getElementById("p").innerHTML = "Entered value is not number";
    }
    else {
        var area_xyz = Math.abs((a * (d - f) + c * (f - a) + e * (b - d)) / 2);
        var sub_area1 = Math.abs((g * (b - d) + a * (d - h) + c * (h - b)) / 2);
        var sub_area2 = Math.abs((g * (d - f) + c * (f - h) + e * (h - d)) / 2);
        var sub_area3 = Math.abs((g * (b - f) + a * (f - h) + e * (h - b)) / 2);
        var total = sub_area1 + sub_area2 + sub_area3;
        if ((area_xyz - total) < 0.00000001) {
            document.getElementById("p").innerHTML = "The choosen Point is :- " + g + "," + h + " is in the triangle";
        }
        else {
            document.getElementById("p").innerHTML = "The choosen Point is :- " + g + "," + h + " is not in the triangle";
        }
    }
}
//# sourceMappingURL=FindPoint.js.map