function Findpoint(){

    let x1: HTMLInputElement = <HTMLInputElement>document.getElementById("x1");
    let y1: HTMLInputElement = <HTMLInputElement>document.getElementById("y1");
    let x2: HTMLInputElement = <HTMLInputElement>document.getElementById("x2");
    let y2: HTMLInputElement = <HTMLInputElement>document.getElementById("y2");
    let x3: HTMLInputElement = <HTMLInputElement>document.getElementById("x3");
    let y3: HTMLInputElement = <HTMLInputElement>document.getElementById("y3");
    let m: HTMLInputElement = <HTMLInputElement>document.getElementById("m");
    let n: HTMLInputElement = <HTMLInputElement>document.getElementById("n");

    var a: number = +x1.value;
    var b: number = +y1.value;
    var c: number = +x2.value;
    var d: number = +y2.value;
    var e: number = +x3.value;
    var f: number = +y3.value;
    var g: number = +m.value;
    var h: number = +n.value;

    if (isNaN(a) || isNaN(b) || isNaN(c)  || isNaN(d) || isNaN(e) || isNaN(f) || isNaN(g) || isNaN(h)){
        document.getElementById("p").innerHTML = "Entered value is not number";
    }else{
        var area_xyz = Math.abs((a*(d-f) + c*(f-a) + e*(b-d)) / 2);
        var sub_area1 = Math.abs((g*(b-d) + a*(d-h) + c*(h-b)) / 2);
        var sub_area2 = Math.abs((g*(d-f) + c*(f-h) + e*(h-d)) / 2);
        var sub_area3 = Math.abs((g*(b-f) + a*(f-h) + e*(h-b)) / 2);

        var total = sub_area1 + sub_area2 + sub_area3;

        if((area_xyz - total) < 0.00000001){
            document.getElementById("p").innerHTML = "The choosen Point is :- " +g+","+h+" is in the triangle";
        }else{
            document.getElementById("p").innerHTML = "The choosen Point is :- " +g+","+h+" is not in the triangle";
        }
    }

}