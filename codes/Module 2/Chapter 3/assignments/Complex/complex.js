function complex() {
    var a = document.getElementById("t1");
    var b = document.getElementById("t2");
    var c = document.getElementById("t3");
    var data = a.value;
    var real;
    var imaginary;
    var p = data.indexOf("+");
    if (p != -1) {
        real = +data.substring(0, p);
        imaginary = +data.substring(p + 1, data.length - 1);
        b.value = "Real Part:-" + real;
        c.value = "Complex Part:-" + imaginary;
    }
    else {
        var i = data.indexOf("i");
        if (i != -1) {
            imaginary = +data.substring(0, data.length - 1);
            b.value = "Real Part:- 0";
            c.value = "Complex Part:-" + imaginary;
        }
        else {
            real = +data.substr(0, data.length);
            b.value = "Real Part:-" + real;
            c.value = "Complex Part:- 0";
        }
    }
}
//# sourceMappingURL=complex.js.map