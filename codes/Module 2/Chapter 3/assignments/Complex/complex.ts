function complex(){
    var a: HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    var b: HTMLInputElement = <HTMLInputElement>document.getElementById("t2");
    var c: HTMLInputElement = <HTMLInputElement>document.getElementById("t3");

    var data: string = a.value;
    var real: number;
    var imaginary: number;

    var p: number = data.indexOf("+");

    if(p!=-1){
        real = +data.substring(0,p);
        imaginary = +data.substring(p+1,data.length-1);
        b.value = "Real Part:-"+real;
        c.value = "Complex Part:-" + imaginary;
    }
    else{

        var i: number = data.indexOf("i");

        if(i != -1){
            imaginary = +data.substring(0,data.length-1);
            b.value = "Real Part:- 0";
            c.value = "Complex Part:-" + imaginary;
        }else{
            real = +data.substr(0,data.length);
            b.value = "Real Part:-"+real;
            c.value = "Complex Part:- 0";
        }
    }
}